import React from "react";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { AddContact, Contact, ContactList } from "./screens";
import store from "./redux/store";

const { Navigator, Screen } = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Navigator headerMode="none">
          <Screen name="Contact list" component={ContactList} />
          <Screen name="Contact" component={Contact} />
          <Screen name="New contact" component={AddContact} />
        </Navigator>
      </NavigationContainer>
    </Provider>
  );
}
