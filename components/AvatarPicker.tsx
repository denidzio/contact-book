import React, { useEffect, useCallback } from "react";
import { StyleSheet } from "react-native";
import { Avatar } from "react-native-elements";
import * as ImagePicker from "expo-image-picker";

export default function AvatarPicker(props: {
  rounded?: boolean;
  title?: string;
  uri?: string;
  size: number | "small" | "medium" | "large" | "xlarge" | undefined;
  onPickedImage: (uri: string) => void;
}) {
  const { rounded, title = "JS", uri, size, onPickedImage } = props;

  const handlePress = useCallback(async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (status === "granted") {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });

      if (!result.cancelled) {
        onPickedImage(result.uri);
      }
    }
  }, []);

  return (
    <Avatar
      rounded={rounded}
      title={title}
      source={uri ? { uri } : undefined}
      size={size}
      onPress={handlePress}
      activeOpacity={0.7}
      containerStyle={styles.avatar}
    />
  );
}

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: "#ccc",
  },
});
