import React, { useState, useEffect, useCallback } from "react";
import {
  NativeSyntheticEvent,
  StyleSheet,
  Button,
  TextInputChangeEventData,
  View,
} from "react-native";
import { Input } from "react-native-elements";
import { isContactChanged } from "../helpers/contact.helper";
import {
  isEmailValid,
  isNameValid,
  isPhoneNumberValid,
} from "../helpers/validation.helper";
import IContact from "../interfaces/IContact";
import IInput from "../interfaces/IInput";

const fields = {
  name: {
    errorMessage: "Not valid name",
    required: true,
  },
  phoneNumber: {
    errorMessage: "Not valid phone number",
    required: true,
  },
  email: {
    errorMessage: "Not valid email",
    required: false,
  },
};

export default function Form(props: {
  contact?: IContact;
  buttonText?: string;
  onSubmit: (contact: IContact) => void;
  onChange?: (isChanged: boolean) => void;
}) {
  const { contact, buttonText = "Save", onSubmit, onChange } = props;

  const [isErrorVisible, setIsErrorVisible] = useState(false);

  const [name, setName] = useState<IInput>({ text: "", error: true });
  const [phoneNumber, setPhoneNumber] = useState<IInput>({ error: true });
  const [email, setEmail] = useState<IInput>({ error: false });

  useEffect(() => {
    if (!contact) {
      return;
    }

    setName({ error: contact.name ? false : true, text: contact.name });
    setPhoneNumber({
      error: contact.phoneNumber ? false : true,
      text: contact.phoneNumber
        ? contact.phoneNumber.split(" ").join("")
        : undefined,
    });

    setEmail({
      error: Boolean(fields.email.required && contact.email),
      text: contact.email,
    });
    setIsErrorVisible(true);
  }, [contact]);

  useEffect(() => {
    if (!onChange) {
      return;
    }

    const localContact = {
      name: name.text as string,
      phoneNumber: phoneNumber.text,
      email: email.text,
    };

    onChange(isContactChanged(contact, localContact));
  }, [name, phoneNumber, email]);

  const handleChangeName = useCallback(
    (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
      const text = e.nativeEvent.text.trim();
      setName({ error: !isNameValid(text), text });
    },
    [name]
  );

  const handleChangePhoneNumber = useCallback(
    (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
      const text = e.nativeEvent.text.trim();
      setPhoneNumber({ error: !isPhoneNumberValid(text), text });
    },
    [phoneNumber]
  );

  const handleChangeEmail = useCallback(
    (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
      const text = e.nativeEvent.text.trim();

      if (!text) {
        setEmail({ error: false, text });
        return;
      }

      setEmail({ error: !isEmailValid(text), text });
    },
    [email]
  );

  const handleSubmit = useCallback(() => {
    if (name.error || phoneNumber.error || email.error) {
      setIsErrorVisible(true);
      return;
    }

    const data: IContact = {
      name: name.text as string,
      phoneNumber: phoneNumber.text,
      email: email.text,
      id: contact?.id,
    };

    onSubmit(data);
  }, [contact, name, phoneNumber, email, onSubmit]);

  return (
    <View style={styles.content}>
      <Input
        leftIcon={{ name: "person", style: styles.icon }}
        value={name.text}
        label="Name"
        placeholder="John Smith"
        errorMessage={
          isErrorVisible && name.error ? fields.name.errorMessage : undefined
        }
        onChange={handleChangeName}
      />
      <Input
        leftIcon={{ name: "phone", style: styles.icon }}
        value={phoneNumber.text}
        label="Phone number"
        placeholder="+380 50 000 0000"
        keyboardType="phone-pad"
        errorMessage={
          isErrorVisible && phoneNumber.error
            ? fields.phoneNumber.errorMessage
            : undefined
        }
        onChange={handleChangePhoneNumber}
      />
      <Input
        leftIcon={{
          name: "email",
          style: styles.icon,
        }}
        value={email.text}
        label="Email"
        placeholder="example@gmail.com"
        errorMessage={
          isErrorVisible && email.error ? fields.email.errorMessage : undefined
        }
        onChange={handleChangeEmail}
      />
      <View style={styles.button}>
        <Button title={buttonText} onPress={handleSubmit} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    marginTop: 10,
  },
  icon: {
    marginRight: 5,
  },
  button: {
    marginTop: 12,
  },
});
