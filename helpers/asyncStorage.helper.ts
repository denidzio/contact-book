import AsyncStorage from "@react-native-async-storage/async-storage";

export const setAsyncStorageItem = async (
  key: string,
  value: any
): Promise<void> => {
  try {
    let validValue: string;

    if (typeof value === "object") {
      validValue = JSON.stringify(value);
    } else {
      validValue = String(value);
    }

    await AsyncStorage.setItem(key, validValue);
  } catch (e) {
    throw e;
  }
};

export const getObjectFromAsyncStorage = async (key: string): Promise<any> => {
  try {
    const data = await AsyncStorage.getItem(key);

    if (!data) {
      return null;
    }

    return JSON.parse(data);
  } catch (e) {
    throw e;
  }
};
