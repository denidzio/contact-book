import IContact from "../interfaces/IContact";

export const parseExpoContacts = (expoContacts: any[]): IContact[] => {
  return expoContacts.map((contact) => {
    const { id, name, emails, phoneNumbers, image } = contact;

    return {
      id: id,
      name: name,
      phoneNumber:
        phoneNumbers && phoneNumbers.length
          ? phoneNumbers[0].number.split(" ").join("")
          : undefined,
      email: emails && emails.length ? emails[0].email : undefined,
      imageUri: image?.uri,
    };
  });
};

export const filterContacts = (
  contacts: IContact[],
  query: string
): IContact[] => {
  const matcher = query.trim().toLowerCase();

  return contacts.filter((contact) => {
    const name = contact.name.toLowerCase();
    const phoneNumber = contact.phoneNumber ? contact.phoneNumber : null;

    if (
      name.includes(matcher) ||
      (phoneNumber && phoneNumber.includes(matcher))
    ) {
      return true;
    }

    return false;
  });
};

export const isContactChanged = (
  contact: IContact | undefined,
  localContact: IContact
): boolean => {
  if (contact) {
    if (contact.name !== localContact.name) {
      return true;
    }

    if (
      contact.phoneNumber &&
      contact.phoneNumber !== localContact.phoneNumber
    ) {
      return true;
    }

    if (!contact.phoneNumber && localContact.phoneNumber) {
      return true;
    }

    if (contact.email && contact.email !== localContact.email) {
      return true;
    }

    if (!contact.email && localContact.email) {
      return true;
    }

    return false;
  }

  return (
    !!localContact.name || !!localContact.phoneNumber || !!localContact.email
  );
};
