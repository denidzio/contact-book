import IContact from "../interfaces/IContact";
import { v4 as uuidv4 } from "uuid";
import {
  setAsyncStorageItem,
  getObjectFromAsyncStorage,
} from "./asyncStorage.helper";

export const getAllContacts = async () => {
  const contacts = await getObjectFromAsyncStorage("contacts");
  return contacts || [];
};

export const replaceContacts = async (contacts: IContact[]) => {
  await setAsyncStorageItem("contacts", contacts || []);
};

export const addContact = async (contact: IContact) => {
  const id = uuidv4();
  const contacts = await getAllContacts();
  const newContact = { ...contact, id };

  await setAsyncStorageItem("contacts", [...contacts, newContact]);
};

export const removeContact = async (id: string) => {
  const contacts = (await getAllContacts()) as IContact[];

  await setAsyncStorageItem(
    "contacts",
    contacts.filter((contact) => contact.id !== id)
  );

  return id;
};

export const getContact = async (id: string) => {
  const contacts = (await getAllContacts()) as IContact[];
  return contacts.find((contact) => contact.id === id);
};

export const updateContact = async (id: string, contact: IContact) => {
  const contacts = (await getAllContacts()) as IContact[];
  const updatedContact = { ...contact, id };

  await setAsyncStorageItem(
    "contacts",
    contacts.reduce(
      (acc: IContact[], item: IContact) =>
        item.id === id ? [...acc, updatedContact] : [...acc, item],
      []
    )
  );
};
