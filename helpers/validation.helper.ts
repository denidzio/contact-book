export const isNameValid = (name: string) => {
  return Boolean(name.length);
};

export const isPhoneNumberValid = (phoneNumber: string) => {
  const regex = /^\+380\d{9}$/;
  return Boolean(phoneNumber.match(regex));
};

export const isEmailValid = (email: string) => {
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return Boolean(email.match(regex));
};
