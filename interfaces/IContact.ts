export default interface IContact {
  id?: string;
  name: string;
  phoneNumber?: string;
  email?: string;
  imageUri?: string;
}
