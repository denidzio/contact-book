export default interface IInput {
  text?: string;
  error: boolean;
}
