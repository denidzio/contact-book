import IStore from "./IStore";

export default interface IState {
  contacts: IStore;
}
