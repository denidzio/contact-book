import IContact from "../../../interfaces/IContact";

export default interface IStore {
  contacts: IContact[];
  filteredContacts: IContact[];
  chosenContact?: IContact;
}
