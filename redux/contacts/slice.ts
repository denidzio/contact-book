import { createSlice } from "@reduxjs/toolkit";
import { IState, IStore } from "./interfaces";
import { getAllContactsThunk } from "./thunks";

const name = "contacts";

const initialState: IStore = {
  contacts: [],
  filteredContacts: [],
};

const contactsSlice = createSlice({
  name,
  initialState,
  reducers: {
    setFilteredContacts(state: IStore, action: any) {
      state.filteredContacts = action.payload;
    },
    setChosenContact(state: IStore, action: any) {
      state.chosenContact = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(getAllContactsThunk.fulfilled, (state, action) => {
      state.contacts = action.payload;
      state.filteredContacts = action.payload;
    });
  },
});

export const { setFilteredContacts, setChosenContact } = contactsSlice.actions;

export const selectContacts = (state: IState) => state.contacts.contacts;
export const selectFilteredContacts = (state: IState) =>
  state.contacts.filteredContacts;
export const selectChosenContact = (state: IState) =>
  state.contacts.chosenContact;

export default contactsSlice.reducer;
