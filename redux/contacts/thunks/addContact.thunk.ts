import { createAsyncThunk } from "@reduxjs/toolkit";
import { addContact } from "../../../helpers/contactStorage.helper";
import IContactItem from "../../../interfaces/IContact";
import { IState } from "../interfaces";

export default createAsyncThunk<void, IContactItem, { state: IState }>(
  "contacts/addContact",
  async (data) => {
    await addContact(data);
  }
);
