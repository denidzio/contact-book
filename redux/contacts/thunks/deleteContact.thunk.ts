import { createAsyncThunk } from "@reduxjs/toolkit";
import { removeContact } from "../../../helpers/contactStorage.helper";
import { IState } from "../interfaces";

export default createAsyncThunk<void, undefined, { state: IState }>(
  "contacts/deleteContact",
  async (_, { getState }) => {
    const editedContactId = getState().contacts.chosenContact?.id;
    editedContactId && (await removeContact(editedContactId));
  }
);
