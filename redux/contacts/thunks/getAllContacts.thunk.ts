import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  getAllContacts,
  replaceContacts,
} from "../../../helpers/contactStorage.helper";
import * as Contacts from "expo-contacts";
import IContact from "../../../interfaces/IContact";
import { parseExpoContacts } from "../../../helpers/contact.helper";

export default createAsyncThunk<IContact[], undefined, any>(
  "contacts/getAllContacts",
  async () => {
    const contacts = await getAllContacts();

    if (contacts.length) {
      return contacts;
    }

    const { status } = await Contacts.requestPermissionsAsync();

    if (status === "granted") {
      const { data } = await Contacts.getContactsAsync();

      const parsedData = parseExpoContacts(data);
      await replaceContacts(parsedData);

      return parsedData;
    }
  }
);
