export { default as getAllContactsThunk } from "./getAllContacts.thunk";
export { default as deleteContactThunk } from "./deleteContact.thunk";
