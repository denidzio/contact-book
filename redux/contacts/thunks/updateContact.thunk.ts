import { createAsyncThunk } from "@reduxjs/toolkit";
import { updateContact } from "../../../helpers/contactStorage.helper";
import IContact from "../../../interfaces/IContact";

export default createAsyncThunk<void, IContact, any>(
  "contacts/updateContact",
  async (contact) => {
    if (contact && contact.id) {
      await updateContact(contact.id, contact);
    }
  }
);
