import { configureStore } from "@reduxjs/toolkit";
import constactsReducer from "./contacts/slice";

export default configureStore({
  reducer: {
    contacts: constactsReducer,
  },
});
