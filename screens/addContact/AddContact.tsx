import React, { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import { StyleSheet, ScrollView } from "react-native";
import { AvatarPicker, ContactForm } from "../../components";
import IContact from "../../interfaces/IContact";
import Header from "./components/Header";
import addContactThunk from "../../redux/contacts/thunks/addContact.thunk";
import { getAllContactsThunk } from "../../redux/contacts/thunks";
import { useNavigation } from "@react-navigation/native";

export default function AddContact() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [layout, setLayout] = useState({ width: 0, height: 0 });
  const [imageUri, setImageUri] = useState("");

  const onPickedImage = useCallback((uri) => {
    setImageUri(uri);
  }, []);

  const handleSave = useCallback(
    async (contact: IContact) => {
      await dispatch(addContactThunk({ ...contact, imageUri })),
        dispatch(getAllContactsThunk()),
        navigation.goBack();
    },
    [imageUri]
  );

  return (
    <ScrollView
      onLayout={(event) => setLayout(event.nativeEvent.layout)}
      stickyHeaderIndices={[0]}
    >
      <Header />
      <AvatarPicker
        size={layout.width}
        onPickedImage={onPickedImage}
        uri={imageUri}
      />
      <ContactForm onSubmit={handleSave} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({});
