import React, { useCallback } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Header as ReactHeader } from "react-native-elements";
import { MaterialIcons } from "@expo/vector-icons";

export default function Header() {
  const navigation = useNavigation();

  const handleBack = useCallback(() => {
    navigation.goBack();
  }, []);

  return (
    <ReactHeader
      leftComponent={
        <MaterialIcons
          name="arrow-back"
          size={24}
          color="#fff"
          onPress={handleBack}
        />
      }
      centerComponent={{
        text: "Add new contact",
        style: styles.title,
      }}
    />
  );
}

const styles = StyleSheet.create({
  title: {
    fontWeight: "normal",
    fontSize: 18,
    width: 150,
    color: "#fff",
  },
});
