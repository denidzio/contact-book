import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, ScrollView, Linking } from "react-native";
import { selectChosenContact } from "../../redux/contacts/slice";
import Header from "./components/Header";
import { ContactForm } from "../../components";
import IContact from "../../interfaces/IContact";
import updateContactThunk from "../../redux/contacts/thunks/updateContact.thunk";
import { getAllContactsThunk } from "../../redux/contacts/thunks";
import { useNavigation } from "@react-navigation/core";

export default function Contact() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const contact = useSelector(selectChosenContact);

  const [imageUri, setImageUri] = useState<string | undefined>();
  const [isChanged, setIsChanged] = useState(false);

  useEffect(() => {
    if (!contact) {
      return;
    }

    setImageUri(contact.imageUri);
  }, [contact]);

  const handleChangeContact = useCallback((isChanged: boolean) => {
    setIsChanged(isChanged);
  }, []);

  const handlePickedImage = useCallback((uri) => {
    if (uri === imageUri) {
      return;
    }

    setImageUri(uri);
    setIsChanged(true);
  }, []);

  const handleSubmit = async (contact: IContact) => {
    if (isChanged) {
      await dispatch(updateContactThunk({ ...contact, imageUri }));
      dispatch(getAllContactsThunk());

      navigation.goBack();
      return;
    }

    if (!contact.phoneNumber) {
      return;
    }

    Linking.openURL(`tel:${contact.phoneNumber}`);
  };

  if (!contact) {
    return null;
  }

  return (
    <ScrollView stickyHeaderIndices={[0]}>
      <Header imageUri={imageUri} onPickedImage={handlePickedImage} />
      <ContactForm
        contact={contact}
        buttonText={isChanged ? "Edit" : "Call"}
        onSubmit={handleSubmit}
        onChange={handleChangeContact}
      />
      <View></View>
    </ScrollView>
  );
}
