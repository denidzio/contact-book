import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import { Header as ReactHeader } from "react-native-elements";
import { MaterialIcons } from "@expo/vector-icons";
import {
  deleteContactThunk,
  getAllContactsThunk,
} from "../../../redux/contacts/thunks";
import { AvatarPicker } from "../../../components";

export default function Header(props: {
  imageUri: string | undefined;
  onPickedImage: (iamgeUri: string) => void;
}) {
  const { imageUri, onPickedImage } = props;

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const handleBack = useCallback(() => {
    navigation.goBack();
  }, []);

  const handleDelete = useCallback(async () => {
    await dispatch(deleteContactThunk());
    dispatch(getAllContactsThunk());

    navigation.goBack();
  }, []);

  const handlePickedImage = useCallback((uri) => {
    onPickedImage(uri);
  }, []);

  return (
    <ReactHeader
      leftContainerStyle={styles.titleContainer}
      leftComponent={
        <MaterialIcons
          name="arrow-back"
          size={24}
          color="#fff"
          onPress={handleBack}
        />
      }
      centerContainerStyle={styles.avatar}
      centerComponent={
        <AvatarPicker
          rounded={true}
          size={50}
          uri={imageUri}
          onPickedImage={handlePickedImage}
        />
      }
      rightContainerStyle={styles.actionsContainer}
      rightComponent={
        <MaterialIcons
          name="delete"
          size={24}
          color="#fff"
          onPress={handleDelete}
        />
      }
    />
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    fontWeight: "normal",
    fontSize: 18,
    width: 100,
    color: "#fff",
  },
  avatar: {
    flexDirection: "row",
    justifyContent: "flex-end",
    position: "relative",
    left: 20,
  },
  actionsContainer: {
    justifyContent: "center",
    position: "relative",
    right: 5,
  },
});
