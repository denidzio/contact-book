import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import Header from "./components/Header";
import List from "./components/List";
import SearchBar from "./components/SearchBar";

export default function ContactList() {
  const [layout, setLayout] = useState({ width: 0, height: 0 });

  return (
    <View
      style={styles.container}
      onLayout={(event) => setLayout(event.nativeEvent.layout)}
    >
      <Header />
      <SearchBar />
      <List width={layout.width} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
