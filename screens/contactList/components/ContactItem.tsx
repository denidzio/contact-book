import React, { useCallback } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { MaterialIcons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";
import IContact from "../../../interfaces/IContact";
import { setChosenContact } from "../../../redux/contacts/slice";

export default function ContactItem({ contact }: { contact: IContact }) {
  const { name, email } = contact;

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handlePressMore = useCallback(() => {
    dispatch(setChosenContact(contact));
    navigation.navigate("Contact");
  }, [contact]);

  return (
    <View style={styles.container}>
      <MaterialIcons name="perm-contact-cal" size={28} color="dodgerblue" />
      <View style={styles.contactInfo}>
        <Text style={styles.name}>{name}</Text>
        {email ? <Text>{email}</Text> : null}
      </View>
      <Button title="More" onPress={handlePressMore} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  contactInfo: {
    marginLeft: 15,
    marginRight: "auto",
  },
  name: {
    fontWeight: "bold",
  },
});
