import React, { useCallback } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Header as ReactHeader } from "react-native-elements";

export default function Header() {
  const navigation = useNavigation();

  const handlePressAdd = useCallback(() => {
    navigation.navigate("New contact");
  }, []);

  return (
    <ReactHeader
      leftComponent={{
        text: "Contact list",
        style: styles.title,
      }}
      rightComponent={{
        icon: "add",
        iconStyle: styles.action,
        onPress: handlePressAdd,
      }}
    />
  );
}

const styles = StyleSheet.create({
  title: {
    fontWeight: "normal",
    fontSize: 18,
    width: 100,
    color: "#fff",
  },
  action: {
    color: "#fff",
  },
});
