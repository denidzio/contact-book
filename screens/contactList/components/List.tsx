import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FlatList, StyleSheet } from "react-native";
import { selectFilteredContacts } from "../../../redux/contacts/slice";
import { getAllContactsThunk } from "../../../redux/contacts/thunks";
import ContactItem from "./ContactItem";

export default function List({ width }: { width: number }) {
  const dispatch = useDispatch();
  const filteredContacts = useSelector(selectFilteredContacts);

  useEffect(() => {
    dispatch(getAllContactsThunk());
  }, []);

  return (
    <FlatList
      style={{ ...styles.list, width }}
      data={filteredContacts}
      renderItem={({ item }) => <ContactItem contact={item} />}
    />
  );
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
  },
});
