import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StyleSheet } from "react-native";
import SearchBarNative from "react-native-elements/dist/searchbar/SearchBar-android";
import { SearchBarBaseProps } from "react-native-elements/dist/searchbar/SearchBar";
import {
  selectContacts,
  setFilteredContacts,
} from "../../../redux/contacts/slice";
import { filterContacts } from "../../../helpers/contact.helper";

const SafeSearchBar =
  SearchBarNative as unknown as React.FC<SearchBarBaseProps>;

export default function SearchBar() {
  const dispatch = useDispatch();
  const contacts = useSelector(selectContacts);
  const [search, setSearch] = useState("");

  const handleChangeText = useCallback(
    (text) => {
      setSearch(text);
    },
    [search]
  );

  useEffect(() => {
    const query = search.trim();

    if (query === "") {
      dispatch(setFilteredContacts(contacts));
      return;
    }

    dispatch(setFilteredContacts(filterContacts(contacts, query)));
  }, [search]);

  return (
    <SafeSearchBar
      platform="android"
      value={search}
      placeholder="Name or phone number"
      onChangeText={handleChangeText}
    />
  );
}

const styles = StyleSheet.create({});
