export { default as Contact } from "./contact/Contact";
export { default as ContactList } from "./contactList/ContactList";
export { default as AddContact } from "./addContact/AddContact";
